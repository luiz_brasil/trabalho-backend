const { DataTypes } = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    name:  {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ratting: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    timestamps: false
});

User.associate = function(models) {
    User.hasOne(models.Cart);
    User.hasMany(models.Product);
};

module.exports = User;