const { DataTypes } = require("sequelize");
const sequelize = require("../config/sequelize");

const Cart = sequelize.define('Cart', {
    
    quantity_of_products: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    amount:  {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    discount: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    payment_method: {
        type: DataTypes.STRING,
        allowNull: false
    },
    shipping: {
        type: DataTypes.DOUBLE,
        allowNull: false
    }

});

Cart.associate = function(models) {
    Cart.belongsToMany(models.Product, { through: 'CartProduct'});
    Cart.belongsTo(models.User);
};

module.exports = Cart;