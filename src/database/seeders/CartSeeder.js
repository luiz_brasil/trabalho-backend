const Cart = require("../../models/Cart");
const faker = require('faker-br');

const seedCart = async function () {
try {
    await Cart.sync({ force: true });

    for (let i = 0; i < 10; i++) {
    await Cart.create({
        quantity_of_products: 0,
        amount: faker.random.number({ min: 1, max: 32 }),
        discount: 10,
        payment_method: faker.finance.transactionType(),
        shipping: 0,
    });
    }
} catch (err) {
    console.log(err);
}
};

module.exports = seedCart;
