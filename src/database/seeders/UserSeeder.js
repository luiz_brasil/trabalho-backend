const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function () {
try {
    await User.sync({ force: true });

    for (let i = 0; i < 10; i++) {
    await User.create({
        cpf: faker.br.cpf(),
        name: faker.name.firstName(),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber(),
        ratting: 5,
        age: faker.random.number({ min: 18, max: 100 }),
        address: faker.address.streetAddress(),
        password: faker.internet.password()
    });
    }
} catch (err) { 
    console.log(err); 
}
};

module.exports = seedUser;
