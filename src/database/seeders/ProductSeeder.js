const Product = require("../../models/Product");
const faker = require('faker-br');

const seedProduct = async function () {
try {
    await Product.sync({ force: true });

    for (let i = 0; i < 25; i++) {
    await Product.create({
        name: faker.commerce.productName(),
        price: faker.commerce.price(30, 800, 2),
        category: faker.commerce.productAdjective(),
        photograph: faker.image.image(),
        description: faker.lorem.text(),
    });
    }
} catch (err) {
    console.log(err);
}
};

module.exports = seedProduct;
