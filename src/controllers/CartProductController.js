const {Op} = require('sequelize');
const Cart = require('../models/Cart');
const Product = require('../models/Product')

const showProducts = async(req,res) => {
    const {id} = req.params;
    try {
        const cart = await Cart.findByPk(id);
        const products = await cart.getProducts();
        return res.status(200).json(products);
    }catch(err){
        return res.status(500).json({err});
    }
};

const showCarts= async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const carts = await product.getCarts();
        return res.status(200).json(carts);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeProducts = async(req,res) => {
    const {id} = req.params;
    try {
        const cart = await Cart.findByPk(id);
        const products = await cart.getProducts();
        await cart.removeProducts(products);
        await cart.update({ quantity_of_products: await cart.countProducts()});
        return res.status(200).json(await cart.getProducts());
    }catch(err){
        return res.status(500).json({err});
    }
};

const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const cart = await Cart.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await cart.addProducts(product);
        await cart.update({ quantity_of_products: await cart.countProducts()});
        return res.status(200).json(await cart.getProducts());
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const cart = await Cart.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await cart.removeProduct(product);
        await cart.update({ quantity_of_products: await cart.countProducts()});
        return res.status(200).json(await cart.getProducts());
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    showProducts,
    showCarts,
    removeProducts,
    addRelationship,
    removeRelationship
}