const { Router } = require("express");
const router = Router();
const UserController=require("../controllers/UserController")
const ProductController = require("../controllers/ProductController");
const CartController = require("../controllers/CartController");
const CartProdController = require("../controllers/CartProductController");

// USER
router.post("/User", UserController.create);
router.get("/User/:id", UserController.show); 
router.get("/User", UserController.index); 
router.put("/User/:id", UserController.update);
router.delete("/User/:id", UserController.destroy);

// PRODUCT
router.post("/Product", ProductController.create);
router.get("/Product/:id", ProductController.show); 
router.get("/Product", ProductController.index); 
router.put("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);
router.put("/Productadduser/:id", ProductController.addRelationship);
router.delete("/Productremoveuser/:id", ProductController.removeRelationship);
router.get("/Productshowcarts/:id", CartProdController.showCarts);

// CART 
router.post("/Cart", CartController.create);
router.get("/Cart/:id", CartController.show); 
router.get("/Cart", CartController.index); 
router.put("/Cart/:id", CartController.update);
router.delete("/Cart/:id", CartController.destroy);
router.put("/Cartadduser/:id", CartController.addRelationship);
router.delete("/Cartremoveuser/:id", CartController.removeRelationship);
router.get("/Cartshowproducts/:id", CartProdController.showProducts);
router.delete("/Cartremoveproducts/:id", CartProdController.removeProducts);
router.post("/Cartaddproduct/:id", CartProdController.addRelationship);
router.delete("/Cartremoveproduct/:id", CartProdController.removeRelationship);

module.exports = router;

